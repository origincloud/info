# OriginCloud | 原点 信息更新
**本项目用于发布最新信息, 请收藏网页或星标**

请同时**收藏**下列网址 **作为备份**:
https://github.com/OriginCloud/info


## 信息更新

### 域名
最新域名 (2025-01-25 更新):
https://www.origincloud0.com

备用域名:
https://origincloud02.xyz

永久域名:
https://origincloud.xyz
https://www.origincloudx.com


#### 以上方法均失效时, 可联系管理员邮箱
邮箱:
origincare@protonmail.com

---
---

## 公告 | Announcement
### **庆祝 乙巳蛇年将近 全站半年付(含)以上88折**

#### ⚠️购买前请认真阅读下面几点说明！

#### ⚠️套餐无法叠加，如果当前账户有套餐，一定要先折算现有套餐再用优惠码买新套餐！！！
#### ⚠️如已买了新套餐，才发现之前老套餐没折算，直接找客服帮忙处理，禁止自行折算！

折算流程：
1. 点击 https://origincloud0.com/user/code 进入钱包界面
2. 下滑到底部 `套餐记录` ，点击现有套餐旁的齿轮可将现套餐折算为余额

购买时请输入:
`SheNianMHdvHnUx`

额度：12% off; 半年付, 年付88折

有效期至: 2025-02-16 21:00 PM (UTC+8)
